﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;



namespace Network_Testing_Client
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TCPpage : Page
    {
        public TCPpage()
        {
            this.InitializeComponent();
            this.LatencyWatch = new Stopwatch();
            this.Latency = "0";
        }

        static string PortNumber = "1337";
        Stopwatch LatencyWatch;
        string Latency;
        static int iters = 100;
        List<double> Timestamps = new List<double>();


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            //Start Client
            this.StartClient();
        }

        



        private async void StartClient()
        {
            double LatencyVal = 0.0;


            try
            {
                // Create the StreamSocket and establish a connection to the echo server.
                using (var streamSocket = new Windows.Networking.Sockets.StreamSocket())
                {
                    // The server hostname that we will be establishing a connection to. In hostname we enter the IP address of the Server
                    var hostName = new Windows.Networking.HostName("10.194.46.235");//10.194.117.33

                    this.clientListBox.Items.Add("client is trying to connect...");

                    await streamSocket.ConnectAsync(hostName, TCPpage.PortNumber);

                    this.clientListBox.Items.Add("client connected");

                    // Send a request to the echo server.
                    string request = "Hello, World!";
                    double LatencyValTotal = 0.0;
                    Stream outputStream = streamSocket.OutputStream.AsStreamForWrite();
                    var streamWriter = new StreamWriter(outputStream);
                    Stream inputStream = streamSocket.InputStream.AsStreamForRead();
                    StreamReader streamReader = new StreamReader(inputStream);
                    double LastInterval = 0.0;

                    for (int i = 1; i <= iters; i++)
                    {
                        await streamWriter.WriteLineAsync(request);
                        await streamWriter.FlushAsync();
                        //Time of send
                        LatencyWatch.Start();

                        if (i % 10 == 0)
                        {
                            this.clientListBox.Items.Add(string.Format("client sent the request: \"{0}\"", request));
                        }

                        // Read data from the echo server.
                        string response;
                        response = await streamReader.ReadLineAsync();
                        streamReader.DiscardBufferedData();
                        inputStream.Flush();
                        //Time of receive back - i.e. latency

                        Timestamps.Add(LatencyWatch.ElapsedMilliseconds);

                        //LatencyWatch.Stop();
                        //maxJitter = Math.Max(maxJitter, Math.Abs(LastInterval - LatencyWatch.ElapsedMilliseconds));
                        //LatencyValTotal += LatencyWatch.ElapsedMilliseconds;
                        //LatencyVal = LatencyValTotal / i;
                        //LatencyWatch.Reset();

                        if (i % 10 == 0)
                        {
                            this.clientListBox.Items.Add(string.Format("client received the response: \"{0}\" ", response));
                            Latency = LatencyVal.ToString();
                            this.clientListBox.Items.Add("Latency: " + this.Latency + "ms after " + i.ToString() + " iterations");
                            //                          this.clientListBox.Items.Add("Current Max Jitter(round): " + maxJitter + "ms after " + i.ToString() + " iterations");
                        }
                    }
                    LatencyWatch.Stop();
                    TimeCalculations();
                    outputStream.Dispose();
                    streamWriter.Dispose();
                    inputStream.Dispose();
                    streamReader.Dispose();
                }


            }
            catch (Exception ex)
            {
                Windows.Networking.Sockets.SocketErrorStatus webErrorStatus = Windows.Networking.Sockets.SocketError.GetStatus(ex.GetBaseException().HResult);
                this.clientListBox.Items.Add(webErrorStatus.ToString() != "Unknown" ? webErrorStatus.ToString() : ex.Message);
            }

        }

        private void TimeCalculations()
        {
            double maxJitter = 0.0;

            for (int i = 1; i < iters; i++)
            {
                Timestamps[i - 1] = Timestamps[i] - Timestamps[i - 1];
                maxJitter = Math.Max(maxJitter, Math.Abs(Timestamps[i - 1] - Timestamps[i]));

            }
            double AvgLatency = Timestamps.Average();

            this.clientListBox.Items.Add("!!!!!!!!!!!!!!Results!!!!!!!!!!!!!!");
            this.clientListBox.Items.Add("Final Average Latency: " + AvgLatency + "ms after " + iters + " iterations");
            this.clientListBox.Items.Add("Max Jitter(round): " + maxJitter + "ms after " + iters + " iterations");
            this.clientListBox.SelectedIndex = this.clientListBox.Items.Count - 1;
            this.clientListBox.ScrollIntoView(this.clientListBox.SelectedItem);
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}

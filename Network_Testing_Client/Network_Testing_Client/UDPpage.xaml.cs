﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Network_Testing_Client
{
    /// <summary>
    /// UDP testing client page
    /// </summary>
    public sealed partial class UDPpage : Page
    {
        public UDPpage()
        {
            this.InitializeComponent();
            this.LatencyWatch = new Stopwatch();
            this.iterCount = 0;
            this.reorderCount = 1000;
            this.reordersOccured = 0;
            this.checkrecent = false;
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        static string serverIP = "10.194.46.235";//10.194.117.33

        static string ClientPortNumber = "1336";
        static string ServerPortNumber = "1337";
        Stopwatch LatencyWatch;

        string Latency;
        static int iters = 10;
        int iterCount;
        int reorderCount;
        int reordersOccured;
        bool checkrecent;
        List<double> Timestamps = new List<double>();

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Start Client
            this.StartClient();
        }

        

        private async void StartClient()
        {
            try
            {
                // Create the DatagramSocket and establish a connection to the echo server.
                var clientDatagramSocket = new Windows.Networking.Sockets.DatagramSocket();

                clientDatagramSocket.MessageReceived += ClientDatagramSocket_MessageReceived;

                // The server hostname that we will be establishing a connection to. In this example, the server and client are in the same process.
                var hostName = new Windows.Networking.HostName(serverIP);

                this.clientListBox.Items.Add("client is about to bind...");

                await clientDatagramSocket.BindServiceNameAsync(UDPpage.ClientPortNumber);

                this.clientListBox.Items.Add(string.Format("client is bound to port number {0}", UDPpage.ClientPortNumber));

                // Send a request to the echo server.
                string request = "Hello, World!";
                
                if (iterCount<iters)
                {
                    using (var serverDatagramSocket = new Windows.Networking.Sockets.DatagramSocket())
                    {
                        using (Stream outputStream = (await serverDatagramSocket.GetOutputStreamAsync(hostName, UDPpage.ServerPortNumber)).AsStreamForWrite())
                        {
                            using (var streamWriter = new StreamWriter(outputStream))
                            {
                                await streamWriter.WriteLineAsync(request);
                                this.LatencyWatch.Start();
                                await streamWriter.FlushAsync();
                            }
                        }
                    }
                    this.clientListBox.Items.Add(string.Format("client sent the request: \"{0}\"", request));
                }

                
                
            }
            catch (Exception ex)
            {
                Windows.Networking.Sockets.SocketErrorStatus webErrorStatus = Windows.Networking.Sockets.SocketError.GetStatus(ex.GetBaseException().HResult);
                this.clientListBox.Items.Add(webErrorStatus.ToString() != "Unknown" ? webErrorStatus.ToString() : ex.Message);
            }
        }
        double LatencyValTotal = 0.0;

        private async void ContinueSending()
        {
            var hostName = new Windows.Networking.HostName(serverIP);
            string request = iterCount.ToString();
            using (var serverDatagramSocket = new Windows.Networking.Sockets.DatagramSocket())
            {
                using (Stream outputStream = (await serverDatagramSocket.GetOutputStreamAsync(hostName, UDPpage.ServerPortNumber)).AsStreamForWrite())
                {
                    using (var streamWriter = new StreamWriter(outputStream))
                    {
                        await streamWriter.WriteLineAsync(request);

                        this.LatencyWatch.Start();

                        await streamWriter.FlushAsync();
                    }
                }
            }
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client sent the request: \"{0}\"", request)));

        }


        private async void ClientDatagramSocket_MessageReceived(Windows.Networking.Sockets.DatagramSocket sender, Windows.Networking.Sockets.DatagramSocketMessageReceivedEventArgs args)
        {
            udpSendHandler(sender, args);
        }

        private async void udpSendHandler(Windows.Networking.Sockets.DatagramSocket sender, Windows.Networking.Sockets.DatagramSocketMessageReceivedEventArgs args)
        {
            string response;
            if (iterCount >= iters)
            {
                using (DataReader dataReader = args.GetDataReader())
                {
                    response = dataReader.ReadString(dataReader.UnconsumedBufferLength).Trim();
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client received the response: \"{0}\"", response)));

                }
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client received the response: \"{0}\"", response)));

                sender.Dispose();

                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add("client closed its socket"));
                return;
            }
            using (DataReader dataReader = args.GetDataReader())
            {

                response = dataReader.ReadString(dataReader.UnconsumedBufferLength).Trim();
                iterCount++;
                Timestamps.Add(LatencyWatch.ElapsedMilliseconds);
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client received the response: \"{0}\"", response)));
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.SelectedIndex = this.clientListBox.Items.Count - 1);
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.ScrollIntoView(this.clientListBox.SelectedItem));


            }
            //iterCount = 100;//Reorder only mode
            if (iterCount < iters)
            {
                if (iterCount % 10 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client received the response: \"{0}\"", response)));
                    //depreciated//await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add("Latency: " + this.Latency + "ms after " + iterCount.ToString() + " iterations"));
                }
                ContinueSending();
                return;
            }




            TimeCalculations();
            reorderingSender();
        }

        private async void TimeCalculations()
        {
            double maxJitter = 0.0;

            for (int i = 1; i < Timestamps.Count; i++)
            {
                Timestamps[i - 1] = Timestamps[i] - Timestamps[i - 1];
                maxJitter = Math.Max(maxJitter, Math.Abs(Timestamps[i - 1] - Timestamps[i]));

            }
            double AvgLatency = Timestamps.Average();

            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add("!!!!!!!!!!!!!!Results!!!!!!!!!!!!!!"));
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add("Final Average Latency: " + AvgLatency + "ms after " + iters + " iterations"));
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add("Max Jitter(round): " + maxJitter + "ms after " + iters + " iterations"));
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.SelectedIndex = this.clientListBox.Items.Count - 1);
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.ScrollIntoView(this.clientListBox.SelectedItem));

        }

        static int reorder = 1000;
        private async void reorderingSender()
        {
            //Hostname = IP of server 
            var hostName = new Windows.Networking.HostName(serverIP);
            int request = 1000;

            var serverDatagramSocket = new Windows.Networking.Sockets.DatagramSocket();
            Stream outputStream = (await serverDatagramSocket.GetOutputStreamAsync(hostName, UDPpage.ServerPortNumber)).AsStreamForWrite();
            var streamWriter = new StreamWriter(outputStream);

            for (int i = 0; i < reorder; i++)
            {
                await streamWriter.WriteLineAsync(request.ToString());

                await streamWriter.FlushAsync();
                if (i % 100 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.clientListBox.Items.Add(string.Format("client sent the request: \"{0}\"", request.ToString())));
                }

                request++;
            }

            streamWriter.Dispose();
            outputStream.Dispose();
            serverDatagramSocket.Dispose();
            


        }
    
    private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}

import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 1338
MESSAGE = "Hello, World!"



sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(bytes(MESSAGE,'utf-8'), (UDP_IP, UDP_PORT))

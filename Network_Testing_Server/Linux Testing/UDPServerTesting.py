import socket

UDP_IP = ''
UDP_PORT = 1338
client_PORT = 1336
iters = 100
iterCount = 0
reorderCount = 1000
try :
    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.bind((UDP_IP, UDP_PORT))

except socket.error as msg :
	print ("Oops?")

print("Socket bound to port ",UDP_PORT)
reorderList = []


def reorderCheck():
    reordersOccured = 0
    checkrecent = False
    responsecount = 1000

    for i in range(0, len(reorderList)):
        if reorderList[i] != responsecount:
            if checkrecent == False:
                reordersOccured += 1
                checkrecent = True
                responsecount += 1
                continue
            if checkrecent == True:
                checkrecent = False

            responsecount += 1
    packetloss = 999 - len(reorderList)
    print("Instances of Reorders found: ", reordersOccured)
    print("Packets lost: ", packetloss)


while True:
    print("Now Listening")
    data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
    datastr = str(data, "utf-8")
    if iterCount >= iters:
        # reorder mode
        reorderList.append(int(datastr))
        reorderCount += 1
        if (reorderCount - 1) % 100 == 0:
            print("Server received message: ", data)
        if reorderCount == 1999:
            reorderCheck()
            print("Server Closing port")
            break

        continue
    sock.sendto(bytes(datastr,'utf-8'), addr)
    if iterCount <= iters:
        iterCount += 1
    if iterCount % 10 == 0:
        print("Server sent back message:", data)
    continue

sock.close()

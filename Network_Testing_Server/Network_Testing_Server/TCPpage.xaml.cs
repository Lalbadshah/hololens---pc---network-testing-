﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Network_Testing_Server
{
    /// <summary>
    /// TCP server testing page
    /// </summary>
    public sealed partial class TCPpage : Page
    {
        public TCPpage()
        {
            this.InitializeComponent();
            this.LatencyWatch = new Stopwatch();
            this.Latency = "0";
        }



        static string PortNumber = "1337";
        Stopwatch LatencyWatch;
        string Latency;
        static int iters = 100;
        List<double> Timestamps = new List<double>();


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Start Server
            this.StartServer();
            
        }

        private async void StartServer()
        {
            try
            {
                var streamSocketListener = new Windows.Networking.Sockets.StreamSocketListener();

                // The ConnectionReceived event is raised when connections are received.
                streamSocketListener.ConnectionReceived += this.StreamSocketListener_ConnectionReceived;

                // Start listening for incoming TCP connections on the specified port. You can specify any port that's not currently in use.
                await streamSocketListener.BindServiceNameAsync(TCPpage.PortNumber);

                this.serverListBox.Items.Add("server is listening...");
            }
            catch (Exception ex)
            {
                Windows.Networking.Sockets.SocketErrorStatus webErrorStatus = Windows.Networking.Sockets.SocketError.GetStatus(ex.GetBaseException().HResult);
                this.serverListBox.Items.Add(webErrorStatus.ToString() != "Unknown" ? webErrorStatus.ToString() : ex.Message);
            }
        }

        private async void StreamSocketListener_ConnectionReceived(Windows.Networking.Sockets.StreamSocketListener sender, Windows.Networking.Sockets.StreamSocketListenerConnectionReceivedEventArgs args)
        {
            var streamReader = new StreamReader(args.Socket.InputStream.AsStreamForRead());
            Stream outputStream = args.Socket.OutputStream.AsStreamForWrite();
            var streamWriter = new StreamWriter(outputStream);

            string request;
            for (int i = 1; i <= iters; i++)
            {



                request = await streamReader.ReadLineAsync();



                if (i % 10 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server received the request: \"{0}\"", request)));
                }

                // Echo the request back as the response.

                await streamWriter.WriteLineAsync(request);
                await streamWriter.FlushAsync();


                if (i % 10 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server sent back the response: \"{0}\"", request)));
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.SelectedIndex = this.serverListBox.Items.Count - 1);
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.ScrollIntoView(this.serverListBox.SelectedItem));
                    //depreciated//await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add("Latency: " + this.Latency + "ms after " + i.ToString() + " iterations"));
                }


            }

            streamReader.Dispose();
            streamWriter.Dispose();
            outputStream.Dispose();

            sender.Dispose();

            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add("server closed its socket"));

            //depreciated//await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add("Latency: " + this.Latency + "ms after " + iters.ToString() + " iterations"));

        }





        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}

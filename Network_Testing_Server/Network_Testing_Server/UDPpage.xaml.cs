﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Network_Testing_Server
{
    /// <summary>
    /// UDP Server Testing Page
    /// </summary>
    public sealed partial class UDPpage : Page
    {
        public UDPpage()
        {
            this.InitializeComponent();

            this.LatencyWatch = new Stopwatch();
            this.iterCount = 0;
            this.reorderCount = 1000;
            this.checkrecent = false;
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        static string ClientPortNumber = "1336";
        static string ServerPortNumber = "1337";
        Stopwatch LatencyWatch;

        string Latency;
        static int iters = 10;
        int iterCount;
        int reorderCount;
        bool checkrecent;
        List<double> Timestamps = new List<double>();

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Start Server
            this.StartServer();
            
        }

        private async void StartServer()
        {
            try
            {
                var serverDatagramSocket = new Windows.Networking.Sockets.DatagramSocket();

                // The ConnectionReceived event is raised when connections are received.
                serverDatagramSocket.MessageReceived += ServerDatagramSocket_MessageReceived;

                this.serverListBox.Items.Add("server is about to bind...");

                // Start listening for incoming TCP connections on the specified port. You can specify any port that's not currently in use.
                await serverDatagramSocket.BindServiceNameAsync(UDPpage.ServerPortNumber);

                this.serverListBox.Items.Add(string.Format("server is bound to port number {0}", UDPpage.ServerPortNumber));
                
            }
            catch (Exception ex)
            {
                Windows.Networking.Sockets.SocketErrorStatus webErrorStatus = Windows.Networking.Sockets.SocketError.GetStatus(ex.GetBaseException().HResult);
                this.serverListBox.Items.Add(webErrorStatus.ToString() != "Unknown" ? webErrorStatus.ToString() : ex.Message);
            }
        }

        List<int> reorderList = new List<int>(); 

        private async void ServerDatagramSocket_MessageReceived(Windows.Networking.Sockets.DatagramSocket sender, Windows.Networking.Sockets.DatagramSocketMessageReceivedEventArgs args)
        {
            string request;
            DataReader dataReader = args.GetDataReader();

            if (iterCount >= iters)
            {//reordering mode

                request = dataReader.ReadString(dataReader.UnconsumedBufferLength).Trim();
                reorderList.Add(Convert.ToInt32(request));
                dataReader.Dispose();
                reorderCount++;



                if ((reorderCount - 1) % 100 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server Recieved the response: \"{0}\"", request)));
                }

                if (reorderCount == 1999)
                {
                    reorderCheck();
                    Stream outputStream = (await sender.GetOutputStreamAsync(args.RemoteAddress, UDPpage.ClientPortNumber)).AsStreamForWrite();
                    var streamWriter = new StreamWriter(outputStream);
                    
                    await streamWriter.WriteLineAsync(request);
                    

                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server sent back the response: \"{0}\"", request)));

                    sender.Dispose();

                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add("server closed its socket"));
                    return;
                }

                return;

            }


            request = dataReader.ReadString(dataReader.UnconsumedBufferLength).Trim();
            dataReader.Dispose();

            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server received the request: \"{0}\"", request)));

            // Echo the request back as the response.
            using (Stream outputStream = (await sender.GetOutputStreamAsync(args.RemoteAddress, UDPpage.ClientPortNumber)).AsStreamForWrite())
            {
                using (var streamWriter = new StreamWriter(outputStream))
                {
                    await streamWriter.WriteLineAsync(request);
                    
                    await streamWriter.FlushAsync();
                }
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server sent back the response: \"{0}\"", request)));

                if (iterCount <= iters) iterCount++;
                //iterCount = 100;//uncomment for reorder only mode
            }
            if (iterCount < iters)
            {
                if (iterCount % 10 == 0)
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("server sent back the response: \"{0}\"", request)));
                    
                }
                return;
            }


        }

        private async void reorderCheck()
        {
            int reordersOccured = 0;
            checkrecent = false;
            int responsecount = 1000;
            for(int i = 0; i < reorderList.Count; i++)
            {
                if (reorderList[i] != responsecount)
                {
                    if (checkrecent == false)
                    {
                        reordersOccured++;
                        checkrecent = true;
                        responsecount++;
                        continue;
                    }

                }
                if (checkrecent == true)
                {
                    checkrecent = false;
                }
                responsecount++;
            }

            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("Instances of Reorders found: \"{0}\"", reordersOccured)));
            int packetsLost = 999 - reorderList.Count;
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.Items.Add(string.Format("Packets lost: \"{0}\"", packetsLost)));
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.SelectedIndex = this.serverListBox.Items.Count - 1);
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => this.serverListBox.ScrollIntoView(this.serverListBox.SelectedItem));

            return;
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
    }
}
